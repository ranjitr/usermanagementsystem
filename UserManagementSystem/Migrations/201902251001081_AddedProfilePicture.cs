namespace UserManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProfilePicture : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDetails", "ProfilePicture", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDetails", "ProfilePicture");
        }
    }
}
