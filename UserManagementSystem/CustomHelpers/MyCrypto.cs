﻿using System;
using System.Security.Cryptography;

namespace UserManagementSystem.CustomHelpers
{
    public static class MyCrypto
    {
        /// <summary>
        /// Method to get hashed password
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string GetHashedPassword(string password)
        {
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(password);
                buffer = md5.ComputeHash(buffer);
                System.Text.StringBuilder hashedPassword = new System.Text.StringBuilder();
                foreach (byte b in buffer)
                {
                    hashedPassword.Append(Convert.ToString(b));
                }
                return Convert.ToString(hashedPassword);
            }
        }
    }
}