﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserManagementSystem.CustomHelpers;
using UserManagementSystem.Models;

namespace UserManagementSystem.Controllers
{
    public class RegisterController : Controller
    {
        //context object.
        private UserDbContext db = new UserDbContext();


        /// <summary>
        /// Get method for Register page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register()
        {
            if (Session["Email"] != null)
            {
                return RedirectToAction("Index", "User");
            }
            ViewBag.successMessage = "";
            List<ContactTypes> contactTypes = db.ContactTypes.ToList();
            ViewBag.contactType = contactTypes;



            return View();
        }


        /// <summary>
        /// Partial view to load when the user clicks on add phone button to load another phone input field.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult _PartialPhone(int index)
        {

            List<ContactTypes> contactTypes = db.ContactTypes.ToList();
            ViewBag.contactType = contactTypes;
            ViewBag.phoneTypeId = "altPhoneType" + index;
            ViewBag.phoneId = "altPhone" + index;
            ViewBag.phoneTypeValidationId = "blankWarningAltPhoneType" + index;
            ViewBag.phoneValidationId = "formatWarningAltPhoneType" + index;

            return PartialView();


        }


        /// <summary>
        /// Method to check if the email is already present in the database or not.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public bool IsEmailAvailable(string Email)
        {
            return !db.UserDetails.Any(user => user.Email == Email);
        }


        /// <summary>
        /// Post method for register to validate user uploaded data and save back to database.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ProfilePicture"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(UserViewModel user, HttpPostedFileBase ProfilePicture)
        {
            if ((ModelState.IsValid) && ((ProfilePicture != null) ? IsValidImage(ProfilePicture) : true) && IsEmailAvailable(user.Email))
            {
                try
                {
                    UserDetails usr = new UserDetails();
                    usr.FirstName = user.FirstName;
                    usr.LastName = user.LastName;
                    usr.Email = user.Email;
                    usr.Password = MyCrypto.GetHashedPassword(user.Password);
                    usr.Address = user.Address;

                    if (ProfilePicture != null)
                    {
                        string imgPath = Path.Combine(Server.MapPath("~/UploadedImages"), user.Email + Path.GetExtension(Path.GetFileName(ProfilePicture.FileName)));
                        ProfilePicture.SaveAs(imgPath);
                        usr.ProfilePicture = "~/UploadedImages/" + user.Email + Path.GetExtension(Path.GetFileName(ProfilePicture.FileName));
                    }

                    db.UserDetails.Add(usr);

                    db.SaveChanges();



                    Contacts contacts = new Contacts();
                    contacts.UserId = usr.UserId;


                    for (int index = 0; index < user.PhoneList.Count; index++)
                    {
                        contacts.Phone = user.PhoneList[index];
                        contacts.ContactType = user.ContactTypeList[index];
                        db.Contacts.Add(contacts);
                        db.SaveChanges();
                    }


                    UserRoles userRoles = new UserRoles();
                    userRoles.UserId = usr.UserId;


                    userRoles.RoleId = db.Roles.Where(u => u.RoleType == "User").Select(u => u.RoleId).Single();
                    db.UserRoles.Add(userRoles);
                    db.SaveChanges();


                    ViewBag.successMessage = "User Registration successful.";
                }
                catch (Exception e)
                {
                    return RedirectToAction("ErrorPage", "User");
                }
            }
            else
            {
                ModelState.AddModelError("", "Some Errors occured.");
            }


            List<ContactTypes> contactTypes = db.ContactTypes.ToList();
            ViewBag.contactType = contactTypes;
            return View(user);
        }

        /// <summary>
        /// Method to validate uploaded file is image file or not
        /// </summary>
        /// <param name="postedFile"></param>
        /// <returns></returns>


        [NonAction]
        public bool IsValidImage(HttpPostedFileBase postedFile)
        {
            if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
           !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
           !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
           !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
           !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}