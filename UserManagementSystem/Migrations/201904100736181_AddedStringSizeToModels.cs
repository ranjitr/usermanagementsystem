namespace UserManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStringSizeToModels : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contacts", "Phone", c => c.String(maxLength: 20));
            AlterColumn("dbo.UserDetails", "FirstName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.UserDetails", "LastName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.UserDetails", "Address", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserDetails", "Address", c => c.String(nullable: false));
            AlterColumn("dbo.UserDetails", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.UserDetails", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Contacts", "Phone", c => c.String());
        }
    }
}
