﻿using System.Linq;
using System.Web.Mvc;
using UserManagementSystem.CustomHelpers;
using UserManagementSystem.Models;

namespace UserManagementSystem.Controllers
{
    public class LoginController : Controller
    {
        //context object.
        private UserDbContext db = new UserDbContext();


        /// <summary>
        /// Get method for Login page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            if (Session["Email"] != null)
            {
                return RedirectToAction("Index", "User");
            }
            return View();
        }

        /// <summary>
        /// Method for Login validation
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(LoginViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }
            else
            {
                var hashedPassword = MyCrypto.GetHashedPassword(user.Password);
                var obj = db.UserDetails.FirstOrDefault(u => u.Email.Equals(user.Email) && u.Password.Equals(hashedPassword));

                if (obj != null)
                {
                    Session["Email"] = user.Email;
                    var roleId = db.UserRoles.Where(u => u.UserId == obj.UserId).Select(u => u.RoleId).First();
                    Session["Role"] = db.Roles.Where(u => u.RoleId == roleId).Select(u => u.RoleType).First();
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Email or Password!");
                    return View(user);
                }
            }
        }
    }
}