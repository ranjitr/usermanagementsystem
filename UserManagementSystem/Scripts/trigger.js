﻿$(document).ready(function () {

    $("#registration").submit(formValidation);

    $("#firstName").keyup(["firstName", "formatWarningFName", "blankWarningFName"], rtName);
    $("#lastName").keyup(["lastName", "formatWarningLName", "blankWarningLName"], rtName);
    $("#email").keyup(["email",  "formatWarningEmail", "blankWarningEmail"], rtEmail);
    $("#email").blur(["email", "notUniqueWarningEmail"], rtEmailUnique);
    $("#password").focus(rtPass);
    $("#phone").keyup(["phone", "formatWarningPhone", "blankWarningPhone"], rtPhone);
    

    $("#address").keyup(["address", "blankWarningAddress"], rtAddress);
    $("#profilePicture").change(["profilePicture", "typeWarningFile"], rtImage);
})