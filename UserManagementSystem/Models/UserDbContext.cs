﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace UserManagementSystem.Models
{
    public class UserDbContext:DbContext
    {
        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<Contacts> Contacts { get; set; }
        public DbSet<ContactTypes> ContactTypes { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
    }
}