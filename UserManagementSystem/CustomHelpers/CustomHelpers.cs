﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UserManagementSystem.CustomHelpers
{
    public static class CustomHelpers
    {
        public static IHtmlString File(this HtmlHelper helper, string id, string name)
        {
            TagBuilder fileTag = new TagBuilder("input");
            fileTag.Attributes.Add("type", "file");
            fileTag.Attributes.Add("id", id);
            fileTag.Attributes.Add("name", name);
            fileTag.Attributes.Add("accept", "image/jpg,image/jpeg,image/pjpeg,image/png,image/x-png");
            return new MvcHtmlString(fileTag.ToString());
        }

        public static IHtmlString Image(this HtmlHelper helper, string src, string alt, string height, string width)
        {
            return Image(helper, src, alt, height, width, null);
        }
        public static IHtmlString Image(this HtmlHelper helper, string src, string alt, string height, string width, object htmlAttributes)
        {
            TagBuilder imgTag = new TagBuilder("img");
            imgTag.Attributes.Add("src", src);
            imgTag.Attributes.Add("alt", alt);
            imgTag.Attributes.Add("height", height);
            imgTag.Attributes.Add("width", width);
            imgTag.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return new MvcHtmlString(imgTag.ToString());
        }
    }
}