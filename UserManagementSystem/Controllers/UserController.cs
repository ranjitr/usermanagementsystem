﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using UserManagementSystem.Filters;
using UserManagementSystem.Models;
using UserManagementSystem.Models.ViewModels;

namespace UserManagementSystem.Controllers
{
    [UserAuthenticationFilter]
    public class UserController : Controller
    {
        //context object.
        private UserDbContext db = new UserDbContext();


        /// <summary>
        /// Action method for Index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (((string)Session["Role"]) == "Admin")
            {
                return RedirectToAction("Index", "Admin");
            }
            List<ContactTypes> contactType = db.ContactTypes.ToList();
            List<KeyValuePair<string, string>> phoneList = new List<KeyValuePair<string, string>>();
            var email = Session["Email"];
            var user = db.UserDetails.First(u => u.Email == email);
            List<Contacts> contacts = db.Contacts.Where(contact => contact.UserId == user.UserId).ToList();
            foreach (var c in contacts)
            {
                var type = contactType.FirstOrDefault(ct => ct.ContactTypeId == c.ContactType).ContactType;
                var phone = c.Phone;
                phoneList.Add(new KeyValuePair<string, string>(type, phone));
            }
            IndexViewModel model = new IndexViewModel();
            model.UserId = user.UserId;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;
            model.Address = user.Address;
            model.ProfilePicture = user.ProfilePicture;
            model.Phone = phoneList;
            return View(model);
        }


        /// <summary>
        /// Method log the user out.
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login", "Login");
        }




        /// <summary>
        /// Partial view to load for navigation bar after the user logs in.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult _HeaderLoggedInNavBar(string email)
        {
            UserDetails user = db.UserDetails.First(u => u.Email == email);
            IndexViewModel model = new IndexViewModel();
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.ProfilePicture = user.ProfilePicture;

            return PartialView(model);
        }





        /// <summary>
        /// Edit method to validate user data and save changes to database.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult Edit(EditViewModel user, int id)
        {
            UserDetails usr = db.UserDetails.SingleOrDefault(u => u.UserId == id);
            if (usr != null && ModelState.IsValid)
            {
                usr.FirstName = user.FirstName;
                usr.LastName = user.LastName;
                usr.Address = user.Address;
                db.Entry(usr).State = EntityState.Modified;
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method for error page
        /// </summary>
        /// <returns></returns>
        public ActionResult ErrorPage()
        {
            return View();
        }


    }
}