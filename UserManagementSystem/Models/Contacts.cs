﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementSystem.Models
{
    public class Contacts
    {
        [Key]
        public int ContactId { get; set; }


        [StringLength(20)]
        [RegularExpression(@"^[0-9]{8,15}$", ErrorMessage = "Phone Number not valid or not in the limit")]
        public string Phone { get; set; }




        [Required(ErrorMessage = "Contact Type is required!")]
        public int ContactType { get; set; }

        [ForeignKey("ContactType")]
        public ContactTypes ContactTypes { get; set; }

        [Required(ErrorMessage = "UserId is required!")]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public UserDetails UserDetails { get; set; }
    }
}