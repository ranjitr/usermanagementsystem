﻿// Get the modal
var modal = $("#myModal");

// Get the button that opens the modal
var btn = $("#editBtn");

// Get the <span> element that closes the modal
var span = $(".close");



btn.click(function () {
    modal.css("display", "block");
})


span.click(function () {
    modal.css("display", "none");
    $("#myModalDetails").css("display", "none");
})

$("#userDataBtn").click(function () {
    window.location.href = "../Admin/UserData";
});

$("#backBtn").click(function () {
    window.history.back();
});

function edit(id) {
    var url = "../Admin/UserEdit/" + id;
    $.get(url, function (data) {
        $("#firstName").val(data["FirstName"]);
        $("#lastName").val(data["LastName"]);
        $("#address").html(data["Address"]);
        $("#userId").val(id);
        modal.css("display", "block");
    });
}

function details(id) {
    var url = "../Admin/UserDetails/" + id;
    $.get(url, function (data) {
        if (data["ProfilePicture"] != null) {
            var src = data["ProfilePicture"].replace("~/", "/");
            $("#imgDivAdmin img").attr("src",src );
        }
        $("#firstNameUser").html(data["FirstName"]);
        $("#lastNameUser").html(data["LastName"]);
        $("#emailUser").html(data["Email"]);
        $("#addressUser").html(data["Address"]);
        $("#phoneDivAdmin").empty();
        $.each(data["Phone"], function (index,object) {
            $("#phoneDivAdmin").append("<p>[" + object.Key + "]</p>");
            $("#phoneDivAdmin").append("<p>" + object.Value + "</p>");
        });
        $("#myModalDetails").css("display", "block");
    });
}

function deleteUser(id){
    if(confirm("Are you sure, you want to delete!")){
        var url = "../Admin/UserDelete/" + id;
        $.get(url, function (data) {

            if (data == "True") {
                alert("User deleted successfully.");
                window.location.reload(true);
            } else {
                $("#failureMessage").html("Couldn't delete, Something Went Wrong.");
                $("#failureMessage").css("display", "block");
            }
        });
    }
}

$("#saveChangesBtn").click(function () {
    var user = new Object();
    user.FirstName = $("#firstName").val();
    user.LastName = $("#lastName").val();
    user.Address = $("#address").val();
    if (user != null) {
        $.ajax({
            type: "POST",
            url: "../User/Edit/" + $("#userId").val(),
            data: {
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
                user:user
            },
            dataType: "json",
            success: function (response) {
                if (response) {
                    alert("User details updated successfully.");
                    window.location.reload(true);
                } else {
                   $("#failureMessage").html("Unable to update data, make sure the data provided is correct.");
                   $("#failureMessage").css("display", "block");
                   modal.css("display", "none");
                }
            },
            failure: function () {
                $("#failureMessage").html("Something went wrong..");
                $("#failureMessage").css("display", "block");
                modal.css("display", "none");
            },
            error: function () {
                $("#failureMessage").css("display", "block");
                modal.css("display", "none");
            }
        });
    }
});