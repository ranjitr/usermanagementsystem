﻿
//Realtime Validation for Name field

function rtName(e) {
    var name = $("#" + e.data[0]).val();
    var letters = /^[a-z]+$/i;
    if (name.match(letters) || name.length == 0) {
        $("#" + e.data[1]).css("display", "none");
        if (name.length == 0) {
            $("#" + e.data[2]).css("display", "block");
        } else {
            $("#" + e.data[2]).css("display", "none");
        }
    }
    else {
        $("#" + e.data[2]).css("display", "none");
        $("#" + e.data[1]).css("display", "block");
    }
}

//Realtime Validation for Email field

function rtEmail(e) {
    var email = $("#" + e.data[0]).val();
    var format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (format.test(email) || email.length == 0) {
        $("#" + e.data[1]).css("display", "none");
        if (email.length == 0) {
            $("#" + e.data[2]).css("display", "block");
        } else {
            $("#" + e.data[2]).css("display", "none");
        }
    }
    else {
        $("#" + e.data[2]).css("display", "none");
        $("#" + e.data[1]).css("display", "block");
    }
}

//Realtime Validation for Unique Email Address

function rtEmailUnique(e){
    var email=$("#"+e.data[0]).val();
    var url="../Register/IsEmailAvailable";
    $.get(url, { Email: email }, function (data) {
            
        if (data=="True") {
            $("#"+e.data[1]).css("display", "none");
        } else {
            $("#"+e.data[1]).css("display","block");
        }
    });
}

//Realtime Validation for Phone number

function rtPhone(e) {
    var phone = $("#" + e.data[0]).val();
    var pattern = /^[0-9]{0,15}$/;
    if ((phone.match(pattern)) || (phone.length == 0)) {
        $("#" + e.data[1]).css("display", "none");
        if (phone.length == 0) {
            $("#" + e.data[2]).css("display", "block");
        } else {
            $("#" + e.data[2]).css("display", "none");
        }
    }
    else {
        $("#" + e.data[2]).css("display", "none");
        $("#" + e.data[1]).css("display", "block");
    }
}


//Realtime Validation for Alternate Phone number

function rtAltPhone(id,validationId) {
    var altPhone = $("#"+id).val();
    var pattern = /^[0-9]{0,15}$/;
    if ((altPhone.match(pattern)) || (altPhone.length == 0)) {
        $("#" + validationId).css("display", "none");
    }
    else {
        $("#" + validationId).css("display", "block");
    }
}


//Realtime Validation for Address field

function rtAddress(e) {
    var uadd = $("#" + e.data[0]).val();
    if (uadd.length == 0) {
        $("#" + e.data[1]).css("display", "block");
    }
    else {
        $("#" + e.data[1]).css("display", "none");
    }
}

//Realtime Validation for Profile Picture field

function rtImage(e) {
    var fileName = $("#"+e.data[0]).val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if (ext == "jpg" || ext == "jpeg" || ext == "pjpeg" || ext == "png" || ext == "x-png" || fileName.length == 0) {
        $("#"+e.data[1]).css("display","none");
    }
    else {
        $("#" + e.data[1]).css("display", "block");
    }
}

//Realtime Validation for Password field

function rtPass() {
    var myInput = $("#password");
    var letter = $("#letter");
    var capital = $("#capital");
    var number = $("#number");
    var length = $("#length");
    var specialCharacter = $("#specialCharacter");

    // When the user clicks on the password field, show the message box

    $("#passwordValidationMessage").css("display", "block");


    // When the user clicks outside of the password field, hide the message box
    myInput.blur(function () {
        $("#passwordValidationMessage").css("display", "none");

    });


    // When the user starts to type something inside the password field
    myInput.keyup(function () {

        if ($("#password").val().length == 0) {
            $("#blankWarningPassword").css("display", "block");
        }
        else {
            $("#blankWarningPassword").css("display", "none");
        }

        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.val().match(lowerCaseLetters)) {
            letter.removeClass("invalid");
            letter.addClass("valid");
        } else {
            letter.removeClass("valid");
            letter.addClass("invalid");
        }

        // Validate uppercase letters
        var upperCaseLetters = /[A-Z]/g;
        if (myInput.val().match(upperCaseLetters)) {
            capital.removeClass("invalid");
            capital.addClass("valid");
        } else {
            capital.removeClass("valid");
            capital.addClass("invalid");
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (myInput.val().match(numbers)) {
            number.removeClass("invalid");
            number.addClass("valid");
        } else {
            number.removeClass("valid");
            number.addClass("invalid");
        }

        // Validate length
        if (myInput.val().length >= 8) {
            length.removeClass("invalid");
            length.addClass("valid");
        } else {
            length.removeClass("valid");
            length.addClass("invalid");
        }

        // Validate special character
        var spl = /[#?!@$%^&*-]/g;
        if (myInput.val().match(spl)) {
            specialCharacter.removeClass("invalid");
            specialCharacter.addClass("valid");
        } else {
            specialCharacter.removeClass("valid");
            specialCharacter.addClass("invalid");
        }
    });
}