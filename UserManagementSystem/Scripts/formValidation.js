﻿
//Validation for Name field

function nameValidation(name) {
    var letters = /^[a-z]+$/i;
    if (name.val().match(letters)) {
        return true;
    }
    else {
        alert("name should only contain characters!");
        name.focus();
        return false;
    }
}

//Validation for Email field

function emailValidation(email) {
    var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.val().length != 0 && pattern.test(email.val())) {
        return true;
    }
    else {
        alert("Email should not be blank or Email format is wrong!");
        email.focus();
        return false;
    }
}

//Validation for Password

function passwordValidation(password) {
    var pattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
    var password_len = password.val().length;
    if ((password_len != 0) && password.val().match(pattern)) {
        return true;
    } else {
        alert("Password should not be empty or password doesn't match given criteria!");
        password.focus();
        return false;
    }

}

//Validation for Password confirmation

function confirmPasswordValidation(confirmPassword, password) {
    if (confirmPassword.val() !== password.val()) {
        alert("Password is not matching!");
        confirmPassword.focus();
        return false;
    }
    return true;
}

//Validation for Phone number

function phoneValidation(phone) {
    var pattern = /^[0-9]{8,15}$/;
    if (phone.val().match(pattern)) {
        return true;
    }
    else {
        alert("phone number must contain numbers only and no less than 8 or more than 15 digits!");
        phone.focus();
        return false;
    }
}

//Validation for Required field(Address)

function requiredFieldValidation(id) {
    if (id.val().length != 0) {
        return true;
    }
    else {
        alert(id.attr("name") + " must have some value!");
        id.focus();
        return false;
    }
}

//Validation for Image field

function imageValidation(file){
    var fileName = file.val();
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if (ext == "jpg" || ext == "jpeg" || ext == "pjpeg" || ext == "png" || ext == "x-png" || fileName.length == 0) {
        return true;
    }
    else {
        alert("Upload image file only!");
        return false;
    }
}

//Function for all the form field validation

function formValidation() {
    var firstName = $("#firstName");
    var lastName = $("#lastName");
    var email = $("#email");
    var password = $("#password");
    var confirmPassword = $("#confirmPassword");
    var phone = $("#phone");
    var address = $("#address");
    var profilePicture = $("#profilePicture");


    if (!nameValidation(firstName))
        return false;
    if (!nameValidation(lastName))
        return false;
    if (!emailValidation(email))
        return false;
    if (!passwordValidation(password))
        return false;
    if (!confirmPasswordValidation(confirmPassword, password))
        return false;
    if (!phoneValidation(phone))
        return false;
    if (!requiredFieldValidation(address))
        return false;
    if (!imageValidation(profilePicture))
        return false;

    return true;
}
