﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserManagementSystem.Models
{
    public class Roles
    {
        [Key]
        public int RoleId { get; set; }
        public string RoleType { get; set; }
    }
}