﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserManagementSystem.Models
{
    public class UserViewModel
    {
        
        [Required(ErrorMessage = "Please enter First Name.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage ="Invalid First Name!")]
        public string FirstName { get; set; }

        
        [Required(ErrorMessage = "Please enter Last Name.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Invalid Last Name!")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Email.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter valid Email Address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = "Password doesn't match given criteria!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm Password.")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password doesn't match.")]
        public string ConfirmPassword { get; set; }

        

        [Required(ErrorMessage = "Please enter Address.")]
        public string Address { get; set; }



        [Required(ErrorMessage = "Please enter Phone Number")]
        [NotMapped]
        public List<string> PhoneList { get; set; }

        [Required(ErrorMessage = "Please select Phone Number Type")]
        [NotMapped]
        public List<int> ContactTypeList { get; set; }

        
    }
}