﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserManagementSystem.Models.ViewModels
{
    public class EditViewModel
    {
        [Required(ErrorMessage = "Please enter First Name.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Invalid First Name!")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter Last Name.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Invalid Last Name!")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Email.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter valid Email Address.")]
        public string Address { get; set; }
    }
}