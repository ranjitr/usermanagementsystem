namespace UserManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactModelUpdate1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contacts", "Phone", c => c.String(maxLength: 15));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contacts", "Phone", c => c.String(nullable: false, maxLength: 15));
        }
    }
}
