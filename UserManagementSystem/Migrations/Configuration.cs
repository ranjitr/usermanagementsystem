namespace UserManagementSystem.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;
    using UserManagementSystem.CustomHelpers;

    internal sealed class Configuration : DbMigrationsConfiguration<UserManagementSystem.Models.UserDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(UserManagementSystem.Models.UserDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.ContactTypes.AddOrUpdate(

                c => c.ContactType,
                new Models.ContactTypes { ContactType = "Home" },
                new Models.ContactTypes { ContactType = "Work" },
                new Models.ContactTypes { ContactType = "Mobile" },
                new Models.ContactTypes { ContactType = "Fax" }
                );

            context.Roles.AddOrUpdate(
                r => r.RoleType,
                new Models.Roles { RoleType = "Admin" },
                new Models.Roles { RoleType = "User" }
                );

            context.UserDetails.AddOrUpdate(
                u => u.Email,
                new Models.UserDetails { FirstName = "Admin", LastName = "Admin", Email = "admin@domain.com", Password = MyCrypto.GetHashedPassword("Admin@123"), Address = "Address", }
                );
            context.SaveChanges();
            context.UserRoles.AddOrUpdate(
                u => u.UserId,
                new Models.UserRoles
                {
                    UserId = context.UserDetails.Where(u => u.Email == "admin@domain.com").Select(u => u.UserId).Single(),
                    RoleId = context.Roles.Where(u => u.RoleType == "Admin").Select(u => u.RoleId).Single()
                }
                );
        }
    }
}
