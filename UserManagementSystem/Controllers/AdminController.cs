﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using UserManagementSystem.Filters;
using UserManagementSystem.Models;
using UserManagementSystem.Models.ViewModels;

namespace UserManagementSystem.Controllers
{
    [UserAuthenticationFilter]
    public class AdminController : Controller
    {
        //context object.
        private UserDbContext db = new UserDbContext();

        /// <summary>
        /// Action method for Index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<ContactTypes> contactType = db.ContactTypes.ToList();
            List<KeyValuePair<string, string>> phoneList = new List<KeyValuePair<string, string>>();
            var email = Session["Email"];
            var user = db.UserDetails.First(u => u.Email == email);
            List<Contacts> contacts = db.Contacts.Where(contact => contact.UserId == user.UserId).ToList();
            foreach (var c in contacts)
            {
                var type = contactType.FirstOrDefault(ct => ct.ContactTypeId == c.ContactType).ContactType;
                var phone = c.Phone;
                phoneList.Add(new KeyValuePair<string, string>(type, phone));
            }
            IndexViewModel model = new IndexViewModel();
            model.UserId = user.UserId;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;
            model.Address = user.Address;
            model.ProfilePicture = user.ProfilePicture;
            model.Phone = phoneList;
            return View(model);
        }

        /// <summary>
        /// Action method for UserData page to load all user details to view to the admin.
        /// </summary>
        /// <returns></returns>
        public ActionResult UserData()
        {
            List<AdminUserViewModel> model = new List<AdminUserViewModel>();
            var roleId = db.Roles.Where(u => u.RoleType == "Admin").Select(u => u.RoleId).Single();
            var userId = db.UserRoles.Where(u => u.RoleId == roleId).Select(u => u.UserId).Single();
            model = db.UserDetails.Where(u => u.UserId != userId).Select(u => new AdminUserViewModel { UserId = u.UserId, FirstName = u.FirstName, LastName = u.LastName, Email = u.Email, Address = u.Address }).ToList();

            return View(model);
        }


        /// <summary>
        /// Method to edit specific user details in modal popup in admin interface.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult UserEdit(int id)
        {
            var user = db.UserDetails.Where(u => u.UserId == id).Select(u => new { u.FirstName, u.LastName, u.Address }).Single();
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method to view specific user details in modal popup in admin interface.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult UserDetails(int id)
        {
            //var user = db.UserDetails.Where(u => u.UserId == id).Select(u => new { u.FirstName, u.LastName, u.Email, u.Address, u.ProfilePicture }).Single();

            List<ContactTypes> contactType = db.ContactTypes.ToList();
            List<KeyValuePair<string, string>> phoneList = new List<KeyValuePair<string, string>>();
            var user = db.UserDetails.First(u => u.UserId == id);
            List<Contacts> contacts = db.Contacts.Where(contact => contact.UserId == user.UserId).ToList();
            foreach (var c in contacts)
            {
                var type = contactType.FirstOrDefault(ct => ct.ContactTypeId == c.ContactType).ContactType;
                var phone = c.Phone;
                phoneList.Add(new KeyValuePair<string, string>(type, phone));
            }
            IndexViewModel model = new IndexViewModel();
            model.UserId = user.UserId;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;
            model.Address = user.Address;
            model.ProfilePicture = user.ProfilePicture;
            model.Phone = phoneList;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method to delete specific user in admin interface.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UserDelete(int id)
        {
            if (id > 0)
            {
                UserDetails user = db.UserDetails.Single(u => u.UserId == id);
                if (user != null)
                {
                    db.Entry(user).State = EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            return (true);
        }
    }
}