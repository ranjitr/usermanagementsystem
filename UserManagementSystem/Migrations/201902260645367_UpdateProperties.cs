namespace UserManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProperties : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Contacts", name: "Contact Type", newName: "ContactType");
            RenameColumn(table: "dbo.UserDetails", name: "First Name", newName: "FirstName");
            RenameColumn(table: "dbo.UserDetails", name: "Last Name", newName: "LastName");
            RenameColumn(table: "dbo.UserDetails", name: "Confirm Password", newName: "ConfirmPassword");
            RenameIndex(table: "dbo.Contacts", name: "IX_Contact Type", newName: "IX_ContactType");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Contacts", name: "IX_ContactType", newName: "IX_Contact Type");
            RenameColumn(table: "dbo.UserDetails", name: "ConfirmPassword", newName: "Confirm Password");
            RenameColumn(table: "dbo.UserDetails", name: "LastName", newName: "Last Name");
            RenameColumn(table: "dbo.UserDetails", name: "FirstName", newName: "First Name");
            RenameColumn(table: "dbo.Contacts", name: "ContactType", newName: "Contact Type");
        }
    }
}
