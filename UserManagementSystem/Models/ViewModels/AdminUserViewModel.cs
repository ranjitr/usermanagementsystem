﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserManagementSystem.Models.ViewModels
{
    public class AdminUserViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}