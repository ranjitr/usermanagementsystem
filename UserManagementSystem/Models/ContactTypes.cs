﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserManagementSystem.Models
{
    public class ContactTypes
    {
        [Key]
        public int ContactTypeId { get; set; }

        public string ContactType { get; set; }
    }
}