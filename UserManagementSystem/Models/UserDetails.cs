﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementSystem.Models
{
    public class UserDetails
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Email.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter valid Email Address.")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [StringLength(100)]
        [Required(ErrorMessage = "Please enter Address.")]
        public string Address { get; set; }

        public string ProfilePicture { get; set; }

    }


}